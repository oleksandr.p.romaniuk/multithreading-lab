package org.example.multithreading.barrier;

public class Barrier {

    private int totalThreads = 0;
    private int count = 0;
    private int released = 0;

    public Barrier(int totalThreads) {
        this.totalThreads = totalThreads;
    }

    public synchronized void await(String threadName) throws InterruptedException {
        System.out.println(threadName + " --- await start count = " + count + ", released = " + released);

        while (count == totalThreads) {
            wait();
        }

        count++;

        if (count == totalThreads) {
            notifyAll();

            released = totalThreads;
        } else {
            while (count < totalThreads) {
                System.out.println(threadName + " start waiting, count = " + count + ", " + totalThreads);
                wait();
                System.out.println(threadName + " end waiting, count = " + count + ", " + totalThreads);
            }
        }

        released--;
        if (released == 0) {
            count = 0;
            notifyAll();
        }

        System.out.println(threadName + " ----await end count = " + count + ", released = " + released);
    }
}
