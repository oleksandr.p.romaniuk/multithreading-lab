package org.example.multithreading.barrier;

public class BarrierDemonstration {
    public static void run() throws InterruptedException {
        final Barrier barrier = new Barrier(3);
        Thread p1 = new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    barrier.await("t 1 1");
                    barrier.await("t 1 2");
                    barrier.await("t 1 3");
                } catch (InterruptedException e) {

                }
            }
        });

        Thread p2 = new Thread(new Runnable() {
            public void run() {
                try {
                    Thread.sleep(500);
                    barrier.await("t 2 1");
                    Thread.sleep(500);
                    barrier.await("t 2 2");
                    Thread.sleep(500);
                    barrier.await("t 2 3");
                } catch (InterruptedException ie) {
                }
            }
        });

        Thread p3 = new Thread(new Runnable() {
            public void run() {
                try {
                    Thread.sleep(1500);
                    barrier.await("t 3 1");
                    Thread.sleep(1500);
                    barrier.await("t 3 2");
                    Thread.sleep(1500);
                    barrier.await("t 3 3");
                } catch (InterruptedException ie) {
                }
            }
        });

        p1.start();
        p2.start();
        p3.start();

        p1.join();
        p2.join();
        p3.join();
    }
}
