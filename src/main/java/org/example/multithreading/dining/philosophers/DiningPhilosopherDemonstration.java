package org.example.multithreading.dining.philosophers;

public class DiningPhilosopherDemonstration {
    public static void run() throws InterruptedException {
        final DiningPhilosophers diningPhilosophers = new DiningPhilosophers();

        Thread p0 = new Thread(() -> {
            try {
                diningPhilosophers.lifecycleOfPhilosopher(0);
            } catch (InterruptedException e) {
                throw new RuntimeException(e);
            }
        });

        Thread p1 = new Thread(() -> {
            try {
                diningPhilosophers.lifecycleOfPhilosopher(1);
            } catch (InterruptedException e) {
                throw new RuntimeException(e);
            }
        });

        Thread p2 = new Thread(() -> {
            try {
                diningPhilosophers.lifecycleOfPhilosopher(2);
            } catch (InterruptedException e) {
                throw new RuntimeException(e);
            }
        });

        Thread p3 = new Thread(() -> {
            try {
                diningPhilosophers.lifecycleOfPhilosopher(3);
            } catch (InterruptedException e) {
                throw new RuntimeException(e);
            }
        });

        Thread p4 = new Thread(() -> {
            try {
                diningPhilosophers.lifecycleOfPhilosopher(4);
            } catch (InterruptedException e) {
                throw new RuntimeException(e);
            }
        });

        p0.start();
        p1.start();
        p2.start();
        p3.start();
        p4.start();

        p0.join();
        p1.join();
        p2.join();
        p3.join();
        p4.join();
    }
}
