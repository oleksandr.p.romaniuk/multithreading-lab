package org.example.multithreading.readwrite.lock;

public class ReadWriteLockDemonstration {

    public static void run() throws InterruptedException {
        final ReadWriteLock readWriteLock = new ReadWriteLock();

        Thread writer1 = new Thread(new Runnable() {
            @Override
            public void run() {
                try {

                    System.out.println("Attempting to acquire write lock in writer1: " + System.currentTimeMillis());
                    readWriteLock.acquireWriteLock();
                    System.out.println("write lock acquired writer1: " + +System.currentTimeMillis());

                    // Simulates write lock being held indefinitely
                    for (; ; ) {
                        Thread.sleep(500);
                    }
                } catch (InterruptedException e) {

                }
            }
        });

        Thread writer2 = new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    System.out.println("Attempting to acquire write lock in writer2: " + System.currentTimeMillis());
                    readWriteLock.acquireWriteLock();
                    System.out.println("write lock acquired writer2: " + System.currentTimeMillis());

                } catch (InterruptedException e) {

                }
            }
        });

        Thread reader1 = new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    readWriteLock.acquireReadLock();
                    System.out.println("Read lock acquired: " + System.currentTimeMillis());

                } catch (InterruptedException e) {

                }
            }
        });

        Thread reader2 = new Thread(new Runnable() {
            @Override
            public void run() {
                System.out.println("Read lock about to release: " + System.currentTimeMillis());
                readWriteLock.releaseReadLock();
                System.out.println("Read lock released: " + System.currentTimeMillis());
            }
        });

        reader1.start();
        writer1.start();
        Thread.sleep(3000);
        reader2.start();
        Thread.sleep(1000);
        writer2.start();
        reader1.join();
        reader2.join();
        writer2.join();
    }
}
