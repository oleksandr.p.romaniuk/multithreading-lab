package org.example.multithreading.readwrite.lock;

public class ReadWriteLock {
    private int readers = 0;
    private boolean writeLock = false;

    synchronized public void acquireWriteLock() throws InterruptedException {
        while (readers != 0 || writeLock) {
            wait();
        }
        writeLock = true;
        notifyAll();
    }

    synchronized public void acquireReadLock() throws InterruptedException {
        while (writeLock) {
            wait();
        }

        readers++;
        notifyAll();
    }

    synchronized public void releaseWriteLock() {
        writeLock = false;
        notifyAll();
    }

    synchronized public void releaseReadLock() {
        readers--;
        notifyAll();
    }
}
