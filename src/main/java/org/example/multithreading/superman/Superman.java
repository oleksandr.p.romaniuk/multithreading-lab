package org.example.multithreading.superman;

public class Superman {
    private Superman() {
    }

    private static class Holder {
        private static final Superman superman = new Superman();
    }

    public Superman getInstance() {
        return Holder.superman;
    }

    public void fly() {
        System.out.println("I am flying!");
    }
}
