package org.example.multithreading.semaphore;

public class DemonstrationCountingSemaphore {

    public static void run() throws InterruptedException{
        CountingSemaphore semaphore = new CountingSemaphore(10);

        Thread thread1 = new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    for (int i = 0; i < 5; i++) {
                        semaphore.acquire();
                        System.out.println("Ping " + i);
                    }
                } catch (InterruptedException e) {

                }
            }
        });

        Thread thread2 = new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    for (int i = 0; i < 5; i++) {
                        semaphore.release();
                        System.out.println("Pong " + i);
                    }
                } catch (InterruptedException e) {

                }
            }
        });

        thread2.start();
        thread1.start();
        thread1.join();
        thread2.join();

    }
}
