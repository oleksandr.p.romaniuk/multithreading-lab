package org.example.multithreading.semaphore;

public class CountingSemaphore {
    private int maxPermits = 0;
    private int givenPermits = 0;

    public CountingSemaphore(int maxPermits) {
        this.maxPermits = maxPermits;
    }

    public synchronized void acquire() throws InterruptedException {
        while (maxPermits == givenPermits) {
            wait();
        }

        givenPermits++;
        notify();
    }

    public synchronized void release() throws InterruptedException {
        while (givenPermits == 0) {
            wait();
        }

        givenPermits--;
        notify();
    }
}
