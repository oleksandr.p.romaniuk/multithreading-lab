package org.example.multithreading.barbershop;

import java.util.HashSet;

public class BarberShopProblemDemonstration {
    public static void run() throws InterruptedException {
        final BarberShopProblem barberShopProblem = new BarberShopProblem();

        HashSet<Thread> set = new HashSet<>();
        Thread barberThread = new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    barberShopProblem.barber();
                } catch (InterruptedException e) {
                    throw new RuntimeException(e);
                }
            }
        });
        barberThread.start();

        for (int i = 0; i < 10; i++) {
            Thread thread = new Thread(new Runnable() {
                @Override
                public void run() {
                    try {
                        barberShopProblem.customerWalksIn();
                    } catch (InterruptedException e) {
                        throw new RuntimeException(e);
                    }
                }
            });
            set.add(thread);
        }

        for (Thread t : set) {
            t.start();
        }

        for (Thread t : set) {
            t.join();
        }

        set.clear();
        Thread.sleep(500);

        for (int i = 0; i < 5; i++) {
            Thread t = new Thread(new Runnable() {
                public void run() {
                    try {
                        barberShopProblem.customerWalksIn();
                    } catch (InterruptedException ie) {

                    }
                }
            });
            set.add(t);
        }
        for (Thread t : set) {
            t.start();
            Thread.sleep(5);
        }

        barberThread.join();
    }
}
