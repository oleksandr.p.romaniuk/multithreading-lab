package org.example.multithreading.join;

public class Join {
    public static void run() throws InterruptedException {

        Thread t3 = new Thread(new Runnable() {
            @Override
            public void run() {

                Thread t2 = new Thread(new Runnable() {
                    @Override
                    public void run() {
                        Thread t1 = new Thread(new Runnable() {
                            @Override
                            public void run() {
                                System.out.println("t1 run");
                            }
                        });
                        t1.start();
                        try {
                            t1.join();
                        } catch (InterruptedException e) {
                            throw new RuntimeException(e);
                        }
                        System.out.println("t2 run");
                    }

                });

                t2.start();
                try {
                    t2.join();

                } catch (InterruptedException e) {
                    throw new RuntimeException(e);
                }
                System.out.println("t3 run");
            }
        });

        t3.start();
        t3.join();

    }
}
