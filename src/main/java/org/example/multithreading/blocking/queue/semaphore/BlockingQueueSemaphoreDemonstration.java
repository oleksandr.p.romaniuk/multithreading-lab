package org.example.multithreading.blocking.queue.semaphore;

import org.example.multithreading.blocking.queue.reentrantlock.BlockingQueueWithMutex;

public class BlockingQueueSemaphoreDemonstration {
    public static void run() throws Exception {
        final BlockingQueueWithSemaphore<Integer> queue = new BlockingQueueWithSemaphore<>(50);

        Thread producer = new Thread(new Runnable() {
            @Override
            public void run() {
                System.out.println("Producer started");
                for (int i = 0; i < 50; i++) {
                    try {
                        queue.enqueue(i);
                    } catch (InterruptedException e) {
                        throw new RuntimeException(e);
                    }
                    System.out.println("Producer enqueued value [ " + i + " ]");
                }

            }
        });

        Thread consumer = new Thread(new Runnable() {
            @Override
            public void run() {
                System.out.println("Consumer started");
                while (true) {
                    Integer value = null;
                    try {
                        value = queue.dequeue();
                    } catch (InterruptedException e) {
                        throw new RuntimeException(e);
                    }
                    System.out.println("Consumer Dequeue Value [ " + value + " ]");
                }
            }
        });

        producer.start();
        //Thread.sleep(1000);
        consumer.start();

        producer.join();
        consumer.join();
    }
}
