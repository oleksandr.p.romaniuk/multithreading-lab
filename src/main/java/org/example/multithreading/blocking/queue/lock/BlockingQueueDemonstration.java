package org.example.multithreading.blocking.queue.lock;

public class BlockingQueueDemonstration {
    public static void run() throws Exception {
        final BlockingQueue<Integer> queue = new BlockingQueue<>(50);

        Thread producer = new Thread(new Runnable() {
            @Override
            public void run() {
                System.out.println("Producer started");
                for (int i = 0; i < 50; i++) {
                    try {
                        queue.enqueue(i);
                        System.out.println("Producer enqueued value [ " + i + " ]");
                    } catch (InterruptedException e) {
                        throw new RuntimeException(e);
                    }
                }

            }
        });

        Thread consumer = new Thread(new Runnable() {
            @Override
            public void run() {
                System.out.println("Consumer started");
                try {
                    for (int i = 0; i < 50; i++) {
                        Integer value = queue.dequeue();
                        System.out.println("Consumer Dequeue Value [ " + value + " ]");
                    }
                } catch (InterruptedException e) {
                    throw new RuntimeException(e);
                }
            }
        });

        producer.start();
        //Thread.sleep(1000);
        consumer.start();

        producer.join();
        consumer.join();
    }
}
