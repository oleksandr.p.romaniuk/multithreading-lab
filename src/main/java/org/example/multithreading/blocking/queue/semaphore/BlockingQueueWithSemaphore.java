package org.example.multithreading.blocking.queue.semaphore;

import java.util.concurrent.Semaphore;

public class BlockingQueueWithSemaphore<T> {

    T[] array;
    //final Lock lock = new ReentrantLock();
    final Semaphore lock = new Semaphore(1);
    int size = 0;
    int capacity;
    int head = 0;
    int tail = 0;

    @SuppressWarnings("unchecked")
    public BlockingQueueWithSemaphore(int capacity) {
        // The casting results in a warning
        array = (T[]) new Object[capacity];
        this.capacity = capacity;
    }

    public T dequeue() throws InterruptedException {
        T item = null;

        lock.acquire();

        while (size == 0) {
            // Release the mutex to give other threads
            lock.release();
            // Reacquire the mutex before checking the
            // condition
            lock.acquire();
        }
        if (head == capacity) {
            head = 0;
        }

        item = array[head];
        array[head] = null;
        head++;
        size--;

        lock.release();
        return item;
    }

    public void enqueue(T item) throws InterruptedException {
        lock.acquire();

        while (size == capacity) {
            // Release the mutex to give other threads
            lock.release();
            // Reacquire the mutex before checking the
            // condition
            lock.acquire();
        }

        if (tail == capacity) {
            tail = 0;
        }

        array[tail] = item;
        size++;
        tail++;

        lock.release();
    }

}
