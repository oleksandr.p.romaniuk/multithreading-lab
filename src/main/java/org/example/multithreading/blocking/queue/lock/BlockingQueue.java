package org.example.multithreading.blocking.queue.lock;

public class BlockingQueue<T> {
    T[] array;
    final Object lock = new Object();
    int size = 0;
    int capacity;
    int head = 0;
    int tail = 0;

    @SuppressWarnings("unchecked")
    public BlockingQueue(int capacity) {
        // The casting results in a warning
        array = (T[]) new Object[capacity];
        this.capacity = capacity;
    }

    public void enqueue(T item) throws InterruptedException {
        synchronized (lock) {

            while (size == capacity){
                lock.wait();
            }

            array[tail] = item;
            tail++;
            size++;
            lock.notify();
        }
    }

    public T dequeue() throws InterruptedException {
        T item = null;
        synchronized (lock) {

            while (size == 0) {
                lock.wait();
            }

            item = array[head];
            array[head] = null;
            head++;
            size--;

            lock.notify();
        }

        return item;
    }
}
