package org.example.multithreading.asynchronousexecution;

public class Executor {
    public void asynchronousExecution(Callback callback) {
        Thread t = new Thread(new Runnable() {
            @Override
            public void run() {
                // Do some useful work
                try {
                    Thread.sleep(5000);
                } catch (InterruptedException ie) {
                }
                callback.done();
            }
        });
        t.start();
    }
}
