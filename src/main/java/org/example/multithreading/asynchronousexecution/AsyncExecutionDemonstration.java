package org.example.multithreading.asynchronousexecution;

import java.util.concurrent.CountDownLatch;

public class AsyncExecutionDemonstration {
    public static void run() throws InterruptedException {
        CountDownLatch latch = new CountDownLatch(1);

        Executor executor = new Executor();
        executor.asynchronousExecution(()-> {
            System.out.println("I am done");
            latch.countDown();
        });

        latch.await();

        System.out.println("main thread exiting...");
    }
}
