package org.example.multithreading.asynchronousexecution;

public interface Callback {
    void done();
}
