package org.example.multithreading.deferred.callback;

import java.util.Comparator;
import java.util.HashSet;
import java.util.PriorityQueue;
import java.util.Random;
import java.util.Set;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.ReentrantLock;

public class DeferredCallbackExecutor {

    PriorityQueue<Callback> executionQueue = new PriorityQueue<>(new Comparator<Callback>() {
        @Override
        public int compare(Callback o1, Callback o2) {
            return (int) (o1.executeAt - o2.executeAt);
        }
    });
    ReentrantLock lock = new ReentrantLock();
    Condition newCallbackArrived = lock.newCondition();

    private long findSleepDuration() {
        long currentTime = System.currentTimeMillis();
        return executionQueue.peek().executeAt - currentTime;
    }

    public void registerCallback(Callback callback) {
        lock.lock();
        executionQueue.add(callback);
        newCallbackArrived.signal();
        lock.unlock();
    }

    public static void run() throws InterruptedException {
        Set<Thread> threads = new HashSet<>();
        final DeferredCallbackExecutor executor = new DeferredCallbackExecutor();

        Thread service = new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    executor.start();
                } catch (InterruptedException e) {
                    throw new RuntimeException(e);
                }
            }
        });
        service.setDaemon(true);
        service.start();

        for (int i = 0; i < 10; i++) {
            Thread thread = new Thread(new Runnable() {
                @Override
                public void run() {
                    Callback cb = new Callback("Hello this is " + Thread.currentThread().getName(), 1);
                    executor.registerCallback(cb);
                }
            });
            thread.setName("Thread_" + (i + 1));
            thread.start();
            threads.add(thread);
            Thread.sleep(1000);
        }

        for (Thread t : threads) {
            t.join();
        }
    }

    public void start() throws InterruptedException {
        long sleepFor;

        while(true) {
            lock.lock();

            while (executionQueue.isEmpty()) {
                newCallbackArrived.await();
            }

            while (!executionQueue.isEmpty()) {
                sleepFor = findSleepDuration();

                if (sleepFor <= 0) {
                    break;
                }

                newCallbackArrived.await(sleepFor, TimeUnit.MILLISECONDS);
            }

            Callback cb = executionQueue.poll();
            System.out.println(
                    "Executed at " + System.currentTimeMillis()/1000 + " required at " + cb.executeAt/1000
                            + ": message:" + cb.message);

            lock.unlock();
        }
    }

    static class Callback {
        long executeAt;
        String message;

        public Callback(String message, long executedAfter) {
            this.message = message;
            this.executeAt = System.currentTimeMillis() + (executedAfter * 1000);
        }
    }
}
