package org.example.multithreading.deferred.callback;

public class DeferredCallbackDemonstration {
    public static void run() throws InterruptedException {
        DeferredCallbackExecutor.run();
    }
}
