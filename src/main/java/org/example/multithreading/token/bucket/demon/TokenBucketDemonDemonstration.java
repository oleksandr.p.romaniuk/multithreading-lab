package org.example.multithreading.token.bucket.demon;

import org.example.multithreading.token.bucket.TokenBucketFilter;

import java.util.HashSet;
import java.util.Set;

public class TokenBucketDemonDemonstration {
    public static void run() throws Exception {

        Set<Thread> threads = new HashSet<>();
        final TokenBucketDemonFilter filter = new TokenBucketDemonFilter(5);

        Thread deamon = new Thread(new Runnable() {
            @Override
            public void run() {
                filter.daemonThread();
            }
        });
        deamon.setDaemon(true);
        deamon.start();


        for (int i = 0; i < 9; i++) {
            Thread thread = new Thread(new Runnable() {
                @Override
                public void run() {
                    try {
                        filter.getToken();
                    } catch (InterruptedException e) {
                        System.out.println(Thread.currentThread().getName() + " : " + e.getMessage());
                    }
                }
            });
            thread.setName("Thread [ " + i + " ]");
            threads.add(thread);
        }

        Thread.sleep(10000);

        for (Thread t : threads) {
            t.start();
        }

        for (Thread t : threads) {
            t.join();
        }
    }
}
