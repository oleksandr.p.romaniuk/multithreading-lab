package org.example.multithreading.token.bucket;

public class TokenBucketFilter {

    private int MAX_TOKENS;
    private long lastRequestTime = System.currentTimeMillis();
    long possibleTokens;

    public TokenBucketFilter(int max) {
        this.MAX_TOKENS = max;
       // this.lastRequestedTime = System.currentTimeMillis();
    }

    synchronized public void getToken() throws InterruptedException {
        this.possibleTokens += (System.currentTimeMillis() - lastRequestTime) / 1000;

        System.out.println("Begin: " + possibleTokens);

        if (possibleTokens > MAX_TOKENS) {
            possibleTokens = MAX_TOKENS;
        }

        if (possibleTokens == 0) {
            Thread.sleep(1000);
        } else {
            this.possibleTokens--;
        }

        this.lastRequestTime = System.currentTimeMillis();
        System.out.println("Granting " + Thread.currentThread().getName() +
                " token at " + (System.currentTimeMillis() / 1000) +
                " possibleTokens " + possibleTokens);
    }
}
