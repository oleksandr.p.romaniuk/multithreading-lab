package org.example.multithreading.token.bucket;

import java.util.HashSet;
import java.util.Set;

public class TokenBucketDemonstration {
    public static void run() throws Exception {

        Set<Thread> threads = new HashSet<>();
        final TokenBucketFilter filter = new TokenBucketFilter(5);

        for (int i = 0; i < 9; i++) {
            Thread thread = new Thread(new Runnable() {
                @Override
                public void run() {
                    try {
                        filter.getToken();
                    } catch (InterruptedException e) {
                        System.out.println(Thread.currentThread().getName() + " : " + e.getMessage());
                    }
                }
            });
            thread.setName("Thread [ " + i + " ]");
            threads.add(thread);
        }

        Thread.sleep(10000);

        for (Thread t : threads) {
            t.start();
        }

        for (Thread t : threads) {
            t.join();
        }
    }
}
