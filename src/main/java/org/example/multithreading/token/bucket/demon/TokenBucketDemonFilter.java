package org.example.multithreading.token.bucket.demon;

public class TokenBucketDemonFilter {

    private int MAX_TOKENS;
    long possibleTokens;

    public TokenBucketDemonFilter(int max) {
        this.MAX_TOKENS = max;
    }

    public void getToken() throws InterruptedException {
        synchronized (this) {
            if (possibleTokens == 0) {
                wait();
            } else {
                this.possibleTokens--;
            }
        }

        System.out.println("Granting " + Thread.currentThread().getName() +
                " token at " + (System.currentTimeMillis() / 1000) +
                " possibleTokens " + possibleTokens);
    }

    public void daemonThread() {
        while (true) {
            synchronized (this) {
                if (possibleTokens < MAX_TOKENS) {
                    possibleTokens++;
                    System.out.println("Demon possibleTokens: " + possibleTokens);
                }
                this.notify();
            }
            try {
                Thread.sleep(1000);
            } catch (InterruptedException ie) {
                // swallow exception
            }

        }
    }
}
