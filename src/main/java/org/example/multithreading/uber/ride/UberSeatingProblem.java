package org.example.multithreading.uber.ride;

import java.util.concurrent.BrokenBarrierException;
import java.util.concurrent.CyclicBarrier;
import java.util.concurrent.Semaphore;
import java.util.concurrent.locks.ReentrantLock;

public class UberSeatingProblem {
    private int republicans = 0;
    private int democrats = 0;

    CyclicBarrier barrier = new CyclicBarrier(4);
    ReentrantLock lock = new ReentrantLock();

    private Semaphore demWaiting = new Semaphore(0);
    private Semaphore repWaiting = new Semaphore(0);

    public void seatDemocrat() throws BrokenBarrierException, InterruptedException {
        boolean rideLeader = false;

        lock.lock();
        democrats++;

        if (democrats == 4) {
            demWaiting.release(3);
            democrats -= 4;
            rideLeader = true;
        } else if (democrats == 2 && republicans >= 2) {
            demWaiting.release(1);
            repWaiting.release(2);
            rideLeader = true;
            democrats -= 2;
            republicans -= 2;
        } else {
            lock.unlock();
            demWaiting.acquire();
        }

        seated();
        barrier.await();

        if (rideLeader) {
            drive();
            lock.unlock();
        }
    }

    public void seatRepublican() throws InterruptedException, BrokenBarrierException {
        boolean rideLeader = false;

        lock.lock();
        republicans++;

        if (republicans == 4) {
            repWaiting.release(3);
            republicans -= 4;
            rideLeader = true;
        } else if (republicans == 2 && democrats >= 2) {
            repWaiting.release(1);
            demWaiting.release(2);
            rideLeader = true;
            democrats -=2;
            republicans -= 2;
        } else {
            lock.unlock();
            repWaiting.acquire();
        }

        seated();
        barrier.await();

        if (rideLeader) {
            drive();
            lock.unlock();
        }
    }

    public void drive() {
        System.out.println("Uber Ride on Its wayyyy... with ride leader " + Thread.currentThread().getName());
        System.out.flush();
    }

    public void seated() {
        System.out.println(Thread.currentThread().getName() + "  seated");
        System.out.flush();
    }

    public void start() {
        System.out.println(Thread.currentThread().getName() + "  start");
    }
}
