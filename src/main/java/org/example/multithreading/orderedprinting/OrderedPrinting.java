package org.example.multithreading.orderedprinting;

public class OrderedPrinting {
    public void printFirst() {
        System.out.println("First");
    }

    public void printSecond() {
        System.out.println("Second");
    }

    public void printThird() {
        System.out.println("Third");
    }
}
