package org.example.multithreading.orderedprinting;

import java.util.concurrent.Semaphore;

public class OrderPrintingDemonstration {
    public static void run() throws InterruptedException {
        OrderedPrinting printing = new OrderedPrinting();
        Semaphore s1 = new Semaphore(0);
        Semaphore s2 = new Semaphore(0);

        Thread t1 = new Thread(new Runnable() {
            @Override
            public void run() {
                printing.printFirst();
                s1.release();
            }
        });

        Thread t2 = new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    s1.acquire();
                    printing.printSecond();
                    s2.release();
                } catch (InterruptedException e) {
                    throw new RuntimeException(e);
                }
            }
        });

        Thread t3 = new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    s2.acquire();
                    printing.printThird();
                } catch (InterruptedException e) {
                    throw new RuntimeException(e);
                }
            }
        });

        t1.start();
        t2.start();
        t3.start();

        t1.join();
        t2.join();
        t3.join();
    }
}
