package org.example.multithreading.unisex.bathroom;

import java.util.concurrent.Semaphore;

public class UnisexBathroom {
    static String WOMAN = "WOMAN";
    static String MAN = "MAN";
    static String NONE = "NONE";

    String isUsedBy = "NONE";
    int empsInBathroom = 0;
    Semaphore maxEmps = new Semaphore(3);

    public void useBathroom(String name) throws InterruptedException {
        System.out.println("\n" + name + " using bathroom. Current employees in bathroom = " + empsInBathroom + " " + System.currentTimeMillis());
        Thread.sleep(3000);
        System.out.println("\n" + name + " done using bathroom " + System.currentTimeMillis());
    }

    public void womanUseBathroom(String name) throws InterruptedException {

        synchronized (this) {
            while (MAN.equals(isUsedBy)) {
                this.wait();
            }

            isUsedBy = WOMAN;
            maxEmps.acquire();
            empsInBathroom++;
        }

        useBathroom(name);
        maxEmps.release();

        synchronized (this) {
            empsInBathroom--;
            if (empsInBathroom == 0) isUsedBy = NONE;
            this.notifyAll();
        }
    }

    public void manUseBathroom(String name) throws InterruptedException {

        synchronized (this) {
            while (WOMAN.equals(isUsedBy)) {
                this.wait();
            }

            isUsedBy = MAN;
            empsInBathroom++;
            maxEmps.acquire();
        }

        useBathroom(name);
        maxEmps.release();

        synchronized (this) {
            empsInBathroom--;
            if (empsInBathroom == 0) isUsedBy = NONE;
            this.notifyAll();
        }
    }
}
