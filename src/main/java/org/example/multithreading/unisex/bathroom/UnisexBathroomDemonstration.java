package org.example.multithreading.unisex.bathroom;

public class UnisexBathroomDemonstration {
    public static void run() throws InterruptedException {
        final UnisexBathroom unisexBathroom = new UnisexBathroom();

        Thread female1 = new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    unisexBathroom.womanUseBathroom("Woman 1");
                } catch (InterruptedException e) {

                }
            }
        });

        Thread male1 = new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    unisexBathroom.manUseBathroom("Man 1");
                } catch (InterruptedException e) {

                }
            }
        });

        Thread male2 = new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    unisexBathroom.manUseBathroom("Man 2");
                } catch (InterruptedException e) {
                    throw new RuntimeException(e);
                }
            }
        });

        Thread male3 = new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    unisexBathroom.manUseBathroom("Man 3");
                } catch (InterruptedException e) {
                    throw new RuntimeException(e);
                }
            }
        });

        Thread male4 = new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    unisexBathroom.manUseBathroom("Man 4");
                } catch (InterruptedException e) {
                    throw new RuntimeException(e);
                }
            }
        });

        female1.start();
        male1.start();
        male2.start();
        male3.start();
        male4.start();

        female1.join();
        male1.join();
        male2.join();
        male3.join();
        male4.join();
    }
}
