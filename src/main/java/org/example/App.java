package org.example;

import org.example.multithreading.asynchronousexecution.AsyncExecutionDemonstration;
import org.example.multithreading.barbershop.BarberShopProblemDemonstration;
import org.example.multithreading.foobar.FooBarDemonstration;
import org.example.multithreading.merge.sort.MultiThreadedMergeSortDemonstration;
import org.example.multithreading.orderedprinting.OrderPrintingDemonstration;

/**
 * Hello world!
 *
 */
public class App 
{
    public static void main( String[] args ) throws Exception
    {
        //System.out.println( "Hello World!" );
        //SumUpExample.runTest();
        //BlockingQueueDemonstration.run();
        //BlockingQueueMutexDemonstration.run();
        //BlockingQueueSemaphoreDemonstration.run();
        //TokenBucketDemonstration.run();
        //TokenBucketDemonDemonstration.run();
        //DeferredCallbackDemonstration.run();
        //DemonstrationCountingSemaphore.run();
        //ReadWriteLockDemonstration.run();
        //UnisexBathroomDemonstration.run();
        //BarrierDemonstration.run();
        //UberSeatingProblemDemonstration.run();
        //Join.run();
        //DiningPhilosopherDemonstration.run();
        //BarberShopProblemDemonstration.run();
        //MultiThreadedMergeSortDemonstration.run();
        //AsyncExecutionDemonstration.run();
        //OrderPrintingDemonstration.run();
        FooBarDemonstration.run();
    }
}
