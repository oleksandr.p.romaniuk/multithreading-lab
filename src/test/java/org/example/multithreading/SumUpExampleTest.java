package org.example.multithreading;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

class SumUpExampleTest {

  @Test
  @DisplayName("Should return the correct sum when called")
  void twoThreadsShouldReturnCorrectSumWhenCalled() throws InterruptedException {
    SumUpExample sumUpExample = new SumUpExample(1, Integer.MAX_VALUE / 2);
    SumUpExample sumUpExample2 =
        new SumUpExample(1 + (Integer.MAX_VALUE / 2), Integer.MAX_VALUE);

    Thread t1 = mock(Thread.class);
    Thread t2 = mock(Thread.class);

    when(t1.getState()).thenReturn(Thread.State.TERMINATED);
    when(t2.getState()).thenReturn(Thread.State.TERMINATED);

    SumUpExample.twoThreads();

    assertEquals(sumUpExample.counter + sumUpExample2.counter, Integer.MAX_VALUE);
  }
}